<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>ActivityPub.Actor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#1da1f2">
        <meta property="og:title" content="Activitypub.actor" />
        <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css"/>
        <link rel="stylesheet" href="/general.css" type="text/css"/>
        <link rel="stylesheet" href="/list.css" type="text/css"/>
    </head>
    <body>
        <div class="content">
            <header>
                <h1>ActivityPub.Actor</h1>
                <p>ActivityPub actor importer from centralized non-ActivityPub social networks to ActivityPub capable ones</p>
            </header>
            <main>
                <p>List of services currently supported :</p>
                <ul id="services">
                    <li class="twitter">
                        <a href="https://twitter.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-twitter" aria-hidden="true"></i> Twitter.com</h2>
                            <h3>twitter.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-on" aria-hidden="true"></i> Online (beta)</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="facebook">
                        <a href="https://facebook.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook.com</h2>
                            <h3>facebook.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="instagram">
                        <a href="https://instagram.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-instagram" aria-hidden="true"></i> Instagram.com</h2>
                            <h3>instagram.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="youtube">
                        <a href="https://youtube.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-youtube" aria-hidden="true"></i> YouTube.com</h2>
                            <h3>youtube.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="vimeo">
                        <a href="https://vimeo.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-vimeo" aria-hidden="true"></i> Vimeo.com</h2>
                            <h3>vimeo.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="twitch">
                        <a href="https://twitch.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-twitch" aria-hidden="true"></i> Twitch.com</h2>
                            <h3>twitch.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="linkedin">
                        <a href="https://linkedin.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin.com</h2>
                            <h3>linkedin.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="github">
                        <a href="https://github.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-github" aria-hidden="true"></i> Github.com</h2>
                            <h3>github.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="stackoverflow">
                        <a href="https://stackoverflow.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-stack-overflow" aria-hidden="true"></i> Stackoverflow.com</h2>
                            <h3>stackoverflow.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="behance">
                        <a href="https://behance.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-behance" aria-hidden="true"></i> Behance.com</h2>
                            <h3>behance.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="pinterest">
                        <a href="https://pinterest.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest.com</h2>
                            <h3>pinterest.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="reddit">
                        <a href="https://reddit.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-reddit" aria-hidden="true"></i> Reddit.com</h2>
                            <h3>reddit.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="medium">
                        <a href="https://medium.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-medium" aria-hidden="true"></i> Medium.com</h2>
                            <h3>medium.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="tumblr">
                        <a href="https://tumblr.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-tumblr" aria-hidden="true"></i> Tumblr.com</h2>
                            <h3>tumblr.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    <li class="wikipedia">
                        <a href="https://wikipedia.activitypub.actor" target="_blank">
                            <h2><i class="fa fa-wikipedia-w" aria-hidden="true"></i> Wikipedia.org</h2>
                            <h3>wikipedia.activitypub.actor</h3>
                            <p><i class="fa fa-toggle-off" aria-hidden="true"></i> WIP</p>
                            <button><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </a>
                    </li>
                    
                </ul>
                <div id="arrow"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></div>
                <h2>How it works</h2>
                <p>Using APIs or simple CURL functions, ActivityPub.Actor will create an ActivityPub actor. Here is an basic example object :</p>
                <pre class="code">
{
"@context": "https://www.w3.org/ns/activitystreams",
 "type": "Person",
 "id": "https://service.activitypub.actor/username",
 "name": "My username",
 "preferredUsername": "username",
 "summary": "An example actor.",
 "inbox": "https://service.activitypub.actor/username/inbox/",
 "outbox": "https://service.activitypub.actor/username/outbox/",
 "followers": "https://service.activitypub.actor/username/followers/",
 "following": "https://service.activitypub.actor/username/following/",
 "liked": "https://service.activitypub.actor/username/liked/"
}</pre>
                <p>The end user will only have to write <span class="code">@username@service.activitypub.actor</span> in his ActivityPub capable social network. You can see a working example of <span class="code">@DavidLibeau@twitter.activitypub.actor</span> by <a href="https://twitter.activitypub.actor/DavidLibeau" target="_blank">clicking here</a>.</p>
                <h2>Learn more</h2>
                <p>
                    <ul>
                        <li><a href="https://www.w3.org/TR/activitypub/" target="_blank">ActivityPub on W3C</a></li>
                    </ul>
                </p>
            </main>
            <footer>
                <p>WIP (beta) - <a href="https://framagit.org/DavidLibeau/activitypubactor" target="_blank">Open source</a> - <a href="https://mastodon.xyz/@David" target="_blank">Dev contact</a></p>
            </footer>
        </div>
    </body>
</html>
